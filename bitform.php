<?php
/*
Plugin Name: Bitrix form
Plugin URI: https://gitlab.com/obobko/bitform
Description: Show power of WP
Version: 0.0.1
Author: Oleksandr Bobko
Author URI: https://gitlab.com/obobko/
*/

defined( 'ABSPATH' ) or die( 'Access denied!' );

if(session_status() == PHP_SESSION_NONE) 
{
	session_start();
}

class bitForm
{
	
	function __construct()
	{
		add_action('wp_enqueue_scripts', array(&$this, 'manage_scripts'), 200);
		add_shortcode('btForm', array(&$this,'addForm'));
		
		add_action( 'wp_ajax_formSend', array(&$this, 'formSend_callback') );
		add_action('wp_ajax_nopriv_formSend', array(&$this,'formSend_callback'));
		
		add_action('admin_init', array(&$this, 'bitform_general_section'));  
		register_setting('general', 'biturl', 'esc_attr');
	}
	
	function manage_scripts()
	{
		wp_register_script('jquery', 'https://code.jquery.com/jquery-3.4.1.min.js');
		wp_enqueue_script( 'jquery' );    

		wp_register_script('bitForm_script', plugins_url() . '/bitform/assets/js/script.js', array('jquery'));
		wp_enqueue_script( 'bitForm_script' );         
		
		wp_register_style('bitForm_style', plugins_url() . '/bitform/assets/css/style.css');     
		wp_enqueue_style( 'bitForm_style' );  		
	}
	
	
function  bitform_general_section() 
{  
    add_settings_section(  
        'bitform_settings_section', // Section ID 
        'Bitrix sync', // Section Title
        array(&$this, 'bitform_options_callback'), // Callback
        'general' // What Page?  This makes the section show up on the General Settings Page
    );

    add_settings_field( // Option 1
        'biturl', // Option ID
        'Bitrix URL', // Label
        array(&$this, 'bitform_textbox_callback'), // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'bitform_settings_section', // Name of our section
        array( // The $args
            'biturl' // Should match Option ID
        )  
    ); 
}
	
	function  bitform_options_callback()
	{ 
		echo '<p>Here you can save all Bitrix related stuff</p>';  
	}

	function bitform_textbox_callback($args)
	{
		$option = get_option($args[0]);
		echo '<input type="text" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" />';
	}
	
	
	function addForm()
	{
		?>
			<form class="bitform" id="bitform" name="bitform">
				<label for="leadname">Name</label>
				<input name="leadname" id="leadname" class="leadname" type="text" placeholder="Name" />
				<label for="email">E-mail</label>
				<input name="email" id="email" class="email" type="text" placeholder="E-mail" />
				<button name="sendok" id="sendok" class="sendok" onclick="sendData()">Submit</button>
			</form>
			<div id="place" style="margin-top:20px;">Result: <span id="resp" class="resp"></span></div>
		<?php
	}
	
	function formSend_callback()
	{
		if(!empty($_POST))
			$data = $_POST;
		elseif(!empty($_GET))
			$data = $_GET;
		else
		{
			echo json_encode(array('result' => false, 'message' => 'No data provided'));
			die();
		}
		
		$biturl = get_option('biturl');
		
		$result = $this->curl($biturl, array('name' => $data['leadname'], 'email' => $data['email']), 'GET', true);
		
		if($result['headers']['http_code'] == 200)
		{
			echo json_encode(array('result' => true, 'message' => $result['body']));
		}
		else
		{
			echo json_encode(array('result' => false, 'message' => 'Error occured. Status: '.$result['headers']['http_code']));
		}
		
		wp_die();
	}
	
	function curl($url, $data = array(), $method = 'POST', $returnHeader = false)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		$result = curl_exec($ch);
		$headers = curl_getinfo($ch);
		curl_close($ch);
		
		if($returnHeader)
		{
			return array('body' => $result, 'headers' => $headers);
		}
		
		return $result;
	}
	
	//This is just for test
	function isAlive($state = true, $lang = 'en')
	{
		$ans['en']['yes'] = 'Yes';
		$ans['en']['no'] = 'No';
		$ans['de']['yes'] = 'Ja';
		$ans['de']['no'] = 'Nein';
		
		if($state)
			return $ans[$lang]['yes'];
		else
			return $ans[$lang]['no'];
	}
		
}

$a = new bitForm();