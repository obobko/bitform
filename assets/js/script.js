function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

function sendData()
{
    var leadname = jQuery('.leadname').val();
	var email = jQuery('.email').val();
	
	event.preventDefault(); 
	
	if(!isValidEmailAddress(email))
	{
		jQuery('.resp').text('Invalid email');
		return;
	}
		
	jQuery.ajax({
		type: "POST",
		data: "action=formSend&leadname="+leadname+"&email="+email,
		url: "/wp-admin/admin-ajax.php",
		success: function(data)
		{   
			data = jQuery.parseJSON(data);
			jQuery('.resp').text(data.message);
		}
	});
}