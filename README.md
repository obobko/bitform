=== Bitrix form ===

1. Upload plugin to /wp-content/plugins directory
2. Activate plugin in /wp-admin/plugins.php
3. Enter URL to send data to at wp-admin/options-general.php at Bitrix sync section. (For ex. http://waithook.com/testing_67)
4. Insert [btForm] shortcode at post / page where you would like to show the form. (You may also use do_shortcode('[btForm]') directry in theme code)
5. Enjoy
